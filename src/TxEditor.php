<?php

/**
 * TxEditor.php
 * (c) Andreas Mueller <webmaster@am-wd.de>
 */

namespace Unclassified;

if (!class_exists('Unclassified\TxLib'))
	require_once __DIR__.'/TxLib.php';

/**
 * As TxLib was created by Yves Goergen and ported to PHP, I started to
 * create a online version of the TxEditor.
 *
 * @package    Tx
 * @author     Andreas Mueller <webmaster@am-wd.de>
 * @copyright  (c) 2016 Andreas Mueller
 * @license    LGPL - http://www.gnu.de/documents/lgpl.de.html
 * @link       https://bitbucket.org/BlackyPanther/txlib
 * @see        http://unclassified.software/de/source/txtranslation
 * @version    v1.0-20160229 | stable; no testcases
 */
class TxEditor extends TxLib {

	/**
	 * The root node for a Tx key tree.
	 * @var TxNode
	 */
	private $root = null;

	/**
	 * Gets all the existing keys.
	 *
	 * @param  string  $lang  The language to get the keys for, if set to null all possible keys will be returned.
	 *
	 * @return  string[]  An array with all keys.
	 */
	public function GetKeys($lang = null) {
		$keys = array();

		if ($lang != null && isset($this->translations[$lang])) {
			foreach ($this->translations[$lang] as $key => $val)
				$keys[] = $key;

			return $keys;
		}

		foreach ($this->translations as $lang => $dict) {
			foreach ($dict as $key => $val) {
				if (!in_array($key, $keys))
					$keys[] = $key;
			}
		}

		return $keys;
	}

	/**
	 * Gets all translations for a key.
	 * 
	 * @param  string  $key The key to receive the translations.
	 * 
	 * @return string[] The assoc. array with all translations.
	 */
	public function GetTranslations($key) {
		$tx = array();
		foreach ($this->GetLanguages() as $lang) {
			$t = $this->T($key, null, $lang);
			$tx[$lang] = (substr($t, 0, 1) == '[') ? '' : $t;
		}

		return $tx;
	}

	/**
	 * Returns the whole dictionary.
	 * 
	 * @return  string[][]  2D array containing the whole dictionary.
	 */
	public function GetDictionary() {
		return $this->translations;
	}

	/**
	 * Creates a HTML list element with all children.
	 *
	 * @param  bool  $icon  A value indicating whether a FontAwesome icon should be added.
	 *
	 * @return  string  List element with all containing sublists.
	 */
	public function GetList($icon = true) {
		$list = '';

		$root = $this->GetTree(true);
		foreach ($root->children as $child) {
			$child->createElement($list, $icon);
		}

		return $list;
	}

	/**
	 * Creates a tree using the keys.
	 *
	 * @param  bool  $refresh  A value indicating whether the tree should be built again.
	 *
	 * @return  TxNode  Root node of the tree.
	 */
	public function GetTree($refresh = false) {
		if ($this->root == null || $refresh) {
			$keys = $this->GetKeys();
			$root = new TxNode('root', 'root');

			foreach ($keys as $key) {
				if (substr($key, -1) == '.')
					continue; // dot at the end => invalid

				$cat = null;
				$ar = explode(':', $key);

				if (count($ar) > 2)
					continue; // more than one colon => invalid

				$full = $key;
				$tx = $this->GetTranslations($full);

				if (count($ar) == 2) {
					// One colon => category
					$cat = $ar[0];

					if ($root->hasChild($cat)) {
							$n = &$root->getRef($cat);
					} else {
						$n = new TxNode($cat, 'category');
						$root->children[] = $n;
					}

					$key = substr($key, strlen($cat) + 1);
				}

				if ($cat == null) {
					$root->addKey($key, $full, $tx);
				} else {
					$n->addKey($key, $full, $tx);
				}
			}

			$this->root = $root;
		}

		return $this->root;
	}



	/**
	 * Sets the translation for a key to a culture.
	 *
	 * @param  string  $culture  Language (culture) to set the translation for as 2 letter code (ISO-639-1).
	 * @param  string  $key      Key to set the translation for.
	 * @param  string  $tx       Translation to set.
	 *
	 * @return void
	 */
	public function SetT($culture, $key, $tx) {
		// Create key on all languages (if not exists)
		foreach ($this->GetLanguages() as $lang) {
			if (!isset($this->translations[$lang][$key]))
				$this->translations[$lang][$key] = '';
		}

		if ($culture != null)
			$this->translations[$culture][$key] = $tx;
	}

	/**
	 * Add a language (culture) to the dictionary.
	 * 
	 * @param  string  $language  2 letter language (culture) code (ISO-639-1)
	 * 
	 * @return void
	 */
	public function AddLanguage($language) {
		// Check if culture already exists
		foreach ($this->GetLanguages() as $lang) {
			if ($lang == $language)
				return;
		}

		// We did not reach the return => language does not exist
		if (!empty($this->default)) {
			foreach ($this->translations[$this->default] as $key => $value)
				$this->translations[$language][$key] = '';
		} else {
			$this->translations[$language] = array();
			$this->default = $language;
		}
	}


	/**
	 * Renames an existing key to a new name.
	 * 
	 * @param  string  $old  Old key name.
	 * @param  string  $new  New key name.
	 * 
	 * @return void
	 */
	public function RenameT($old, $new) {
		$tmp = $this->GetTranslations($old);
		$this->UnsetT($old);

		foreach ($tmp as $k => $v)
			$this->SetT($k, $new, $v);
	}



	/**
	 * Unsets (deletes) a key from the dictionary.
	 * 
	 * @param  string  $key  Key to delete.
	 */
	public function UnsetT($key) {
		foreach ($this->GetLanguages() as $lang)
			unset($this->translations[$lang][$key]);
	}

	/**
	 * Unsets (deletes) a language from the dictionary.
	 * 
	 * @param string $language 2 letter language (culture) code (ISO-639-1).
	 * 
	 * @return void
	 */
	public function UnsetLanguage($language) {
		unset($this->translations[$language]);

		if ($this->default == $language)
			$this->default = (count($this->translations) == 0) ? '' : key($this->translations);
	}

	/**
	 * Reads a *.php file into an array and parses it for the dictionary.
	 *
	 * @param  string  $file  The absolute path to the .php file.
	 *
	 * @return void
	 */
	protected function parsePhp($file) {
		$f = file($file);

		$inline = false;
		$lines = array();

		foreach ($f as $line) {
			$l = trim($line);

			if (!$inline) {
				$lines[count($lines)] = '';
				if (substr($line, 0, 5) == '$tx["')
					$inline = true;
			}

			$lines[count($lines)-1] .= $line;

			if (substr($l, -2) == '";')
				$inline = false;
		}

		foreach ($lines as $line) {
			$line = trim($line);
			if (preg_match('/^\$tx\["([a-z]{2})"\]\["(.*)"\] ?= ?"(\X*)";$/', $line, $matches)) {
				$culture = $matches[1];
				$key = $matches[2];
				$tx = $matches[3];
				$this->translations[$culture][$key] = str_replace('\"', '"', $tx);
			}

			if (preg_match('/^\$tx\_default ?= ?"(.*)";/', $line, $matches) && empty($this->default))
				$this->default = $matches[1];
		}
	}
}


/**
 * TxNode implements all features to build a tree with Tx keys.
 *
 * @package    Tx
 * @author     Andreas Mueller <webmaster@am-wd.de>
 * @copyright  (c) 2016 Andreas Mueller
 * @license    LGPL - http://www.gnu.de/documents/lgpl.de.html
 * @link       https://bitbucket.org/BlackyPanther/txlib
 * @version    v1.0-20160229 | stable; no testcases
 */
class TxNode {

	/**
	 * The name of the Node.
	 * @var string
	 */
	public $name;

	/**
	 * The type of the node.
	 * @var string
	 */
	public $type;

	/**
	 * The full key name the node is representing.
	 * @var string
	 */
	public $full;

	/**
	 * The translations found for the key.
	 * @var string[]
	 */
	public $tx;

	/**
	 * All children of the TxNode.
	 * @var TxNode[]
	 */
	public $children;

	/**
	 * Initializes a new instance of TxNode.
	 *
	 * @param  string  $name                      The name of the TxNode.
	 * @param  string  $type  [default = 'node']  The type of the node.
	 *
	 * @return TxNode
	 */
	function __construct($name, $type = 'node') {
		$this->name = $name;
		$this->type = $type;
		$this->full = null;
		$this->tx = null;
		$this->children = array();
	}

	/**
	 * Checks whether the node has a child with the name.
	 *
	 * @param  string  $name  The name of the child.
	 *
	 * @return  bool  true if the child exists, otherwise false.
	 */
	public function hasChild($name) {
		foreach ($this->children as $child) {
			if ($child->name == $name)
				return true;
		}

		return false;
	}

	/**
	 * Returns the reference to a node.
	 *
	 * @param  string  $name  The name of the child for which the reference should be returned.
	 *
	 * @return &TxNode The reference to the node.
	 */
	public function &getRef($name = null) {
		if ($name == null)
			return $this;

		for ($i = 0; $i < count($this->children); $i++) {
			if ($this->children[$i]->name == $name)
				return $this->children[$i];
		}

		throw new \ArgumentException('Child with this name not existing');
	}

	/**
	 * Adds a key to the node and sets the full key if the path matches.
	 *
	 * @param  string  $key   The (partial) key to add to the node.
	 * @param  string  $full  The full key as it was at the beginning.
	 * @param  string  $tx    The translation for the key.
	 *
	 * @return void
	 */
	public function addKey($key, $full, $tx) {
		if (empty($key))
			return;

		$ar = explode('.', $key);

		$key = substr($key, strlen($ar[0]));
		if (substr($key, 0, 1) == '.')
			$key = substr($key, 1);

		foreach ($this->children as $child) {
			if ($child->name == $ar[0]) {
				if (empty($key)) {
					$child->full = $full;
					$child->tx = $tx;
				} else {
					$child->addKey($key, $full, $tx);
				}

				return;
			}
		}

		$n = new TxNode($ar[0]);
		if (empty($key)) {
			$n->full = $full;
			$n->tx = $tx;
		}

		$this->children[] = $n;

		$n->addKey($key, $full, $tx);
	}

	/**
	 * Creates an Html list element with its sublists.
	 *
	 * @param  string  &$list      The reference to the string to write all elements into.
	 * @param  bool    $with_icon  A value indicating whether the FontAwesome icons should be used.
	 *
	 * @return void
	 */
	public function createElement(&$list, $with_icon = true) {
		$list .= '<li>';

		if ($this->full == null && $this->type == 'category') {
			if ($with_icon) $list .= '<span class="fa fa-fw fa-th-large"></span> ';
			$list .= $this->name;
		} else if ($this->full == null) {
			if ($with_icon) $list .= '<span class="fa fa-fw fa-angle-right"></span> ';
			$list .= $this->name;
		} else {
			$list .= '<a href="#" class="tx-key" tx-key="'.$this->full.'">';
			if ($with_icon) $list .= '<span class="fa fa-fw fa-'.(count($this->children) > 0 ? 'angle-right' : 'key fa-rotate-180').'"></span> ';
			$list .= $this->name.'</a>';
		}

		if (count($this->children) > 0) {
			$list .= '<ul>';

			foreach ($this->children as $child)
				$child->createElement($list, $with_icon);

			$list .= '</ul>';
		}

		$list .= '</li>';
	}

	/**
	 * Returns the translation to the given key in the defined language.
	 *
	 * @param  string   $key          The key for the translated text.
	 * @param  mixed[]  $placeholder  Assoc. array with the placeholders of the data.
	 *
	 * @return  string  The translated text of the key.
	 */
	public function T($key, $placeholder = array()) {
		if ($this->full == $key) {
			$t = $this->tx;
			foreach ($placeholder as $key => $value)
				$t = str_replace('{'.$key.'}', $value, $t);
			
			return $t;
		}

		foreach ($this->children as $child) {
			$res = $child->T($key, $placeholder);
			if ($res != null)
				return $res;
		}

		return null;
	}

}

?>