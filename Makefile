# Directories
DOCUMENTATION = docs
SOURCE = src
TOOLS = tools
TESTS = tests

.PHONY: all test docs clean

all: clean test docs

test:
	#php tools/phpunit.phar --verbose tests/*Test.php

docs: clean
	mkdir $(DOCUMENTATION)
	php $(TOOLS)/phpDocumentor.phar -p --template="clean" -d $(SOURCE)/ -t $(DOCUMENTATION)/

clean:
	rm -rf $(DOCUMENTATION)/
