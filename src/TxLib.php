<?php

/**
 * TxLib.php
 * (c) Andreas Mueller <webmaster@am-wd.de>
 */

namespace Unclassified;

/**
 * Translation class. As TxLib is for C# and .NET.
 *
 * All rights reseved to Unclassified!
 * Author of original TxLib: Yves Goergen @ http://unclassified.software.
 * To build translation files, use TxEditor from unclassified.software
 * or TxOnline at https://txonline.am-wd.de (personal project).
 *
 * TxLib: http://unclassified.software/de/source/txtranslation
 *
 * @package    Tx
 * @author     Andreas Mueller <webmaster@am-wd.de>
 * @copyright  (c) 2015-2016 Andreas Mueller
 * @license    LGPL - http://www.gnu.de/documents/lgpl.de.html
 * @link       https://bitbucket.org/BlackyPanther/txlib
 * @see        http://unclassified.software/de/source/txtranslation
 * @version    v1.0-20160229 | stable; no testcases
 */
class TxLib {

	/**
	 * The array with all possible translations.
	 * @var string[]
	 */
	protected $translations;

	/**
	 * The default language as fallback.
	 * @var string
	 */
	protected $default;

	/**
	 * The current selected language.
	 * @var string
	 */
	protected $language;

	/**
	 * Initializes a new instance of TxLib.
	 */
	function __construct() {
		$this->translations = array();
		$this->default = '';
		$this->language = '';
	}

	/**
	 * Loads all *.xml and *.txd files from the given directory and parses them.
	 *
	 * @param  string  $dir  The directory to search the files in.
	 *
	 * @return  bool  true on success, otherwise false.
	 */
	public function LoadDirectory($dir) {
		if (!is_dir($dir))
			return false;

		if (substr($dir, -1) != '/')
			$dir .= '/';

		$d = opendir($dir);
		while ($file = readdir($d)) {
			if (preg_match("/(\.(([a-z]{2})([-][a-z]{2})?))?\.(?:xml|txd|tx\.php)$/", $file)) {
				$this->LoadFile($dir.$file);
			}
		}
		closedir($d);

		return true;
	}

	/**
	 * Loads the dictionary from a specific file.
	 * This may override existing entries!
	 *
	 * @param  string  $file  The absolute path to the file.
	 */
	public function LoadFile($file) {
		switch (substr($file, -3)) {
			case 'txd':
				$this->parseTxd($file);
				break;
			case 'xml':
				$this->parseXml($file);
				break;
			case 'php':
				$this->parsePhp($file);
				break;
			default:
				// currently no error detection
				break;
		}
	}

	/**
	 * Returns the translation to the given key in the defined language.
	 *
	 * @param  string   $key          The key for the translated text.
	 * @param  mixed[]  $placeholder  Assoc. array with the placeholders of the data.
	 * @param  string   $culture      2 letter language (culture) code to get specific translation.
	 * @param  bool     $backup       If set to true, the default language will be used as backup.
	 *
	 * @return  string  The translated text of the key.
	 */
	public function T($key, $placeholder = null, $culture = null, $backup = false) {
		$lang = ($culture == null) ? $this->GetLanguage() : $culture;

		// Try to extract the key from the dictionary
		$text = empty($this->translations[$lang][$key]) ? '' : $this->translations[$lang][$key];

		if (empty($text) && $backup)
			$text = empty($this->translations[$this->default][$key]) ? '' : $this->translations[$this->default][$key];

		if (empty($text))
			$text = '['.$key.']';

		// replace our placeholders by their text
		if ($placeholder != null) {
			foreach ($placeholder as $key => $value)
				$text = str_replace('{'.$key.'}', $value, $text);
		}

		return $text;
	}

	/**
	 * Gets the current language used for the translation.
	 *
	 * @return  string  2 letter language (culture) code (ISO-639-1).
	 */
	public function GetLanguage() {
		// do we have a language set ?
		$lang = $this->language;
		// if not, try to get it from our server
		if (empty($lang))
			$lang = empty($_SERVER['HTTP_ACCEPT_LANGUAGE']) ? '' : substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);

		$lang = strtolower($lang);

		// validate, if language is present
		$found = false;
		foreach ($this->translations as $code => $dict) {
			if ($code == $lang)
				$found = true;
		}

		// fallback => use default language
		if (!$found)
			$lang = $this->default;

		return $lang;
	}

	/**
	 * Gets the default language used as backup.
	 *
	 * @return  string  2 letter language (culture) code (ISO-639-1).
	 */
	public function GetDefaultLanguage() {
		return $this->default;
	}

	/**
	 * Gets all languages (cultures) currently existing at the dictionary.
	 *
	 * @return  string[]  Array with 2 letter language (culture) codes (ISO-639-1).
	 */
	public function GetLanguages() {
		$langs = array();

		foreach ($this->translations as $culture => $key)
			$langs[] = $culture;

		return $langs;
	}

	/**
	 * Sets the default language.
	 *
	 * @param  string  $language  2 letter language (culture) code (ISO-639-1).
	 * 
	 * @return void
	 */
	public function SetDefaultLanguage($language) {
		if (strlen($language) == 2) {
			$this->default = strtolower($language);
		}
	}

	/**
	 * Sets the current language, e.g. caused by a user's input.
	 *
	 * @param  string  $language  2 letter language (culture) code (ISO-639-1).
	 * 
	 * @return void
	 */
	public function SetLanguage($language) {
		if (strlen($language) == 2) {
			$this->language = strtolower($language);
		}
	}
	
	/**
	 * Parses a *.txd file to the dictionary.
	 *
	 * @param  string  $file  The absolute path to the .txd file.
	 *
	 * @return void
	 */
	protected function parseTxd($file) {
		$xml = new \XMLReader();
		$xml->open($file);

		// save current culture
		$culture = '';

		// read step by step the whole file
		while ($xml->read()) {
			// we're only interested into elements
			if ($xml->nodeType == \XMLReader::ELEMENT) {
				// we have reached a new culture
				if ($xml->name == 'culture') {
					// set it to current culture
					$culture = strtolower($xml->getAttribute('name'));
					// check if it has the primary attribute
					// if so, it will be our default language
					if ($xml->getAttribute('primary')) {
						$this->default = $culture;
					}
				}

				// we have our text tags => these are our translations!
				if ($xml->name == 'text') {
					$key = $xml->getAttribute('key');
					$this->translations[$culture][$key] = $xml->readString();
				}
			}
		}

		$xml->close();
	}

	/**
	 * Parses a *.xml file to the dictionary.
	 * The language (culture) code has to be in the filename.
	 *
	 * @param  string  $file  The absolute path to the .txd file.
	 *
	 * @return void
	 */
	protected function parseXml($file) {
		// extract culture from filename
		// e.g. tx.de.xml, tx.en.xml
		$tmp = explode('.', $file);
		$culture = strtolower($tmp[count($tmp)-2]);

		$xml = new \XMLReader();
		$xml->open($file);

		// and here we go!
		// same procedure as on parseTxd
		// BUT culture element is missing
		while ($xml->read()) {
			if ($xml->nodeType == \XMLReader::ELEMENT) {
				if ($xml->name == 'translation') {
					if ($xml->getAttribute('primary')) {
						$this->default = $culture;
					}
				}

				if ($xml->name == 'text') {
					$key = $xml->getAttribute('key');
					$this->translations[$culture][$key] = $xml->readString();
				}
			}
		}

		$xml->close();
	}

	/**
	 * Includes a Php file and transfers the dictionary.
	 *
	 * Be careful which files you give to this function.
	 * It will be included!!!
	 * Therefore bad code may (will) be executed!
	 *
	 * @param  string  $file  Absolute path to php file.
	 *
	 * @return void
	 */
	protected function parsePhp($file) {
		include_once $file;

		$this->default = $tx_default;
		$this->translations = $tx;
	}
}

?>