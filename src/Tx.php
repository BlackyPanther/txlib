<?php

/**
 * Tx.php
 * (c) Andreas Mueller <webmaster@am-wd.de>
 */

namespace Unclassified;

if (!class_exists('Unclassified\TxLib'))
	require_once __DIR__.'/TxLib.php';

/**
 * Translation class. As TxLib is for C# and .NET.
 *
 * All rights reseved to Unclassified!
 * Author of original TxLib: Yves Goergen @ http://unclassified.software.
 *
 * @package    Tx
 * @author     Andreas Mueller <webmaster@am-wd.de>
 * @copyright  (c) 2016 Andreas Mueller
 * @license    LGPL - http://www.gnu.de/documents/lgpl.de.html
 * @link       https://bitbucket.org/BlackyPanther/txlib
 * @see        http://unclassified.software/de/source/txtranslation
 * @version    v1.0-20160229 | stable
 */
class Tx {

	/**
	 * TxLib object to get all the translations.
	 * @var TxLib
	 */
	private static $tx = null;

	/**
	 * Loads all *.xml and *.txd files from the given directory and parses them.
	 *
	 * @param  string  $dir  The directory to search the files in.
	 *
	 * @return  bool  true on success, otherwise false.
	 */
	public static function LoadDirectory($dir) {
		if (self::$tx == null)
			self::$tx = new TxLib();

		return self::$tx->LoadDirectory($dir);
	}

	/**
	 * Loads the dictionary from a specific file.
	 * This may override existing entries!
	 *
	 * @param  string  $file  The absolute path to the file.
	 */
	public static function LoadFile($file) {
		if (self::$tx == null)
			self::$tx = new TxLib();

		self::$tx->LoadFile($file);
	}

	/**
	 * Returns the translation to the given key in the defined language.
	 *
	 * @param  string   $key          The key for the translated text.
	 * @param  mixed[]  $placeholder  Assoc. array with the placeholders of the data.
	 *
	 * @return  string  The translated text of the key.
	 */
	public static function T($key, $placeholder = array()) {
		return self::$tx->T($key, $placeholder);
	}

	/**
	 * Returns the translation to given the key in the defined language with HTML encoded chars.
	 *
	 * @param  string   $key         The key for the translated text.
	 * @param  mixed[]  $placeholder Assoc. array with placeholders of the data.
	 *
	 * @return  string  The translated text of the key as HTML.
	 */
	public static function H($key, $placeholder = array()) {
		// get text
		$txt = self::T($key, $placeholder);
		// encode all special chars
		$encoded = htmlentities($txt);
		// and convert new-lines
		$html = nl2br($encoded);

		return $html;
	}

	/**
	 * Sets the default language.
	 *
	 * @param  string  $language  2 letter language (culture) code (ISO-639-1).
	 * 
	 * @return void
	 */
	public static function SetDefaultLanguage($language) {
		self::$tx->SetDefaultLanguage($language);
	}

	/**
	 * Sets the current language, e.g. caused by a user's input.
	 *
	 * @param  string  $language  2 letter language (culture) code (ISO-639-1).
	 * 
	 * @return void
	 */
	public static function SetLanguage($language) {
		self::$tx->SetLanguage($language);
	}

	/**
	 * Gets the current language used for the translation.
	 *
	 * @return  string  2 letter language (culture) code (ISO-639-1).
	 */
	public static function GetLanguage() {
		return self::$tx->GetLanguage();
	}

	/**
	 * Gets all languages (cultures) currently existing at the dictionary.
	 *
	 * @return  string[]  Array with 2 letter language (culture) codes (ISO-639-1).
	 */
	public static function GetLanguages() {
		return self::$tx->GetLanguages();
	}
}

?>