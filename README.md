# TxLib

-----

The original Library is written in C# for .NET applications and as it is a very nice possibillity to handle a multilingual application, I tried to port it into PHP.

Original TxLib @[Unclassified.software](http://unclassified.software/de/source/txtranslation).

At the moment it only includes some basic functions to handle translations.

An online TxEditor is build as a personal project and published at [TxOnline.am-wd.de](https://txonline.am-wd.de).
Therefore the class has been extended to fit the needs used by the editor.

All code to handle the editor and translations can be found in this repository.

## Example

```php
<?php

// Load the class to handle a site translation.
// It will automatically load the TxLib.
require_once __DIR__."/TxLib/src/Tx.php";

// As you wish, you can define the Unclassified\Tx as Tx itself
// and 'remove' the namespace (as the using directive in C#).
use Unclassified\Tx as Tx;

// Load all files in the direcotry lang (relative to this file).
Tx::LoadDirectory(__DIR__."/lang/");

// You can also specify the file you want to load as dictionary
// The php files can be created with Online TxEditor.
Tx::LoadFile(__DIR__."/Dictionary.tx.php");

// Use a translation
echo Tx::T('Key.Subkey.Text');

?>
```

-----

### LICENSE

The original is licensed under [LGPL](http://www.gnu.de/documents/lgpl.de.html). So this piece of code too.
